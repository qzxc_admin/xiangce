import fetch from 'utils/fetch1';

export function fetchAdList(query) {
  return fetch({
    url: '/v1/ad/ads',
    method: 'get',
    params: query
  });
}

export function fetchPv(pv) {
  return fetch({
    url: '/article_table/pv',
    method: 'get',
    params: { pv }
  });
}
