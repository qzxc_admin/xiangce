/**
 * Created by Administrator on 2017/7/8.
 */
import fetch from 'utils/fetch1';

export function fetchUserList(query) {
    return fetch({
        url: '/v1/user/user/agentuser',
        method: 'get',
        params: query
    });
}

export function updateUser(data,id) {
    return fetch({
        url: '/v1/user/user/updateuser',
        method: 'post',
        params:{id},
        data: data
    });
}
